/**
 * Created by macbookpro on 01.11.16.
 */
var wH = $(window).height();
var wW = $(window).width();
var videoWrap = $('.video-bg');
var mainBlock = $('.slider-wrap');
var video = $('#video');
var curTime = video[0].currentTime;

function Videoconst() {
    this.init = function(){
        video[0].oncanplaythrough = function(){
            video[0].play();
        }
    };
    this.play = function(){
        video[0].play();
    };

    this.pause = function(){
        video[0].pause();
    };
    this.reverse = function(){
    };
    this.videostop = function(tVal){
        var curTime = video[0].currentTime;
        tVal = $('.swiper-slide.swiper-slide-active').attr('data-time');
        if(video[0].currentTime >= tVal){
            video[0].pause();
        }
    }
};
var videon = new Videoconst();

window.onload = function() {
    console.log(wH);
    videoWrap.css('height', wH);
    mainBlock.css('height', wH);
    videon.play();
    // videon.videostop();
    // var viTime = video[0].currentTime;
    // var videooo = document.getElementById("video");
    // TweenMax.fromTo(videooo, 5, {currentTime: 0}, {currentTime:5, ease:Linear.easeNone})

    // setInterval(function(){
    //     console.log(videooo.currentTime);
    // }, 1000);
    video.on('timeupdate', function(){
        videon.videostop();
        var tValSlide = $('.swiper-slide.swiper-slide-active').attr('data-time')
        if(video[0].currentTime >= tValSlide){
            video[0].pause();
        }
    });
    var slider = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        direction: 'vertical',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        mousewheelControl: true
    });
    slider.on('SlideChangeStart', function(){
        var viTime = video[0].currentTime;
        var tValSlide = $('.swiper-slide.swiper-slide-active').attr('data-time');
        console.log('change');
        videon.play();
        slider.lockSwipes();
        setTimeout(function () {
            slider.unlockSwipes();
        }, (tValSlide - video[0].currentTime) * 1000);
        console.log((tValSlide - viTime));
    });

};
$(document).ready(function(){
    console.log('document ready');
});
$('.vid-play').on('click', function(){
    videon.play();
    console.log('play');
});
$('.vid-pause').on('click', function(){
    videon.pause();
});


$('.vid-playback').on('click', function(){
    var videooo = document.getElementById("video");
    var viTime = video[0].currentTime;
    video[0].pause();
    console.log(viTime);
    video[0].currentTime = 4;
    // TweenMax.fromTo(video[0], 4,
    //     {
    //         currentTime: 4
    //     },
    //     {
    //         onUpdate: function () {
    //             console.log(video[0].currentTime)
    //         },
    //         useFrames: false,
    //         currentTime:0,
    //         delay: 0,
    //         ease:Power0.easeNone
    //     }
    //     );
    intervalRewind = setInterval(function(){
        // video[0].playbackRate = 1.0;
        if (video[0].currentTime == 0) {
            clearInterval(intervalRewind);
            video[0].pause();
        } else {
            video[0].currentTime -= 0.1;
        }
    }, 100);
    setInterval(function(){
        console.log(video[0].currentTime);
    }, 1000);
    console.log('playback');
});

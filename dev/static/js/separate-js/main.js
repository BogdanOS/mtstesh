/**
 * Created by macbookpro on 04.11.16.
 */
var videoSlides = [];
var video = $('#video');


function VideoSlide(start, end) {
    this.start = start;
    this.end = end;
}

function processVideoSlide(swiper, backward) {
    var index = swiper.activeIndex;

    if (index < videoSlides.length) {
        var videoSlide = videoSlides[index];

        swiper.lockSwipes();
        video[0].pause();
        video.off('timeupdate');

        if (backward) {
            reversePlayback(swiper, videoSlide.start);
        } else {
            video.on('timeupdate', function() {
                if (video[0].currentTime >= videoSlide.end) {
                    video[0].pause();
                    swiper.unlockSwipes();
                }
            });
            video[0].play();
        }
    }
}

function reversePlayback(swiper, stopPlaybackTIme) {
    intervalRewind = setInterval(function() {
        video[0].playbackRate = 1.0;
        if (video[0].currentTime <= stopPlaybackTIme) {
            clearInterval(intervalRewind);
            video[0].pause();
            swiper.unlockSwipes();
        } else {
            video[0].currentTime -= 0.1;
        }
    }, 50);
}

window.onload = function () {
    var wH = $(window).height();
    var wW = $(window).width();
    var videoWrap = $('.video-bg');
    var mainBlock = $('.slider-wrap');
    var video = $('#video');
    videoWrap.css('height', wH);
    mainBlock.css('height', wH);

    videoSlides.push(new VideoSlide(0, 4));
    videoSlides.push(new VideoSlide(4, 17));
    videoSlides.push(new VideoSlide(17, 27));
    videoSlides.push(new VideoSlide(27, 39));
    videoSlides.push(new VideoSlide(39, 45));
    videoSlides.push(new VideoSlide(45, 54));
    videoSlides.push(new VideoSlide(54, 60));
    var slider = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        direction: 'vertical',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        mousewheelControl: true,

        onInit: function (swiper) {
            processVideoSlide(swiper, false);
        },

        onSlideNextStart: function(swiper) {
            processVideoSlide(swiper, false);
        },

        onSlidePrevStart: function(swiper) {
            processVideoSlide(swiper, true);
        }
    });
};
window.onresize = function() {
    var wH = $(window).height();
    var wW = $(window).width();
    var videoWrap = $('.video-bg');
    var mainBlock = $('.slider-wrap');
    var video = $('#video');
    videoWrap.css('height', wH);
    mainBlock.css('height', wH);
};